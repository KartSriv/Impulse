@echo off
cls
color a
title Impulse
echo Hello, User! This is Impulse, your Autopilot.
pause
title Impulse - 2.1.1
color 0a
set load=
set/a loadnum=0

:Loading
set load=%load%��
cls
echo Hello, User! This is Impulse, your Autopilot.
echo Press any key to continue . . .
echo. 
echo Ploting RootKit . . .
echo ----------------------------------------
echo %load%
echo ----------------------------------------
timeout /t 02 /nobreak >nul

set/a loadnum=%loadnum% +1
if %loadnum%==20 goto Done

goto Loading
:Done
cls
echo Hello, User! This is Impulse, your Autopilot.
echo Press any key to continue . . .
echo. 
echo RootKit has successfully plotted!
echo ----------------------------------------
echo %load%
echo ----------------------------------------
pause
echo STATUS: System has successfully compromised!
set /p id="Please enter the username: "
set /p password="Please enter the password: "
timeout /t 02 /nobreak >nul
systeminfo | findstr /C:"OS"
pause
echo "Adding a user called %id%:-"
timeout /t 02 /nobreak >nul
net user %id% %password% /add
echo "Making %id% user admin:-"
timeout /t 02 /nobreak >nul
net localgroup administrators %id% /add
timeout /t 02 /nobreak >nul
pause
echo This computer has been successfully opened!
echo The username is %id%.
echo The password is %password%.
pause
echo Logging out in && timeout /t 10 
tsdiscon
pause
